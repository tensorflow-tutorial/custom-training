import os
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

# print("TensorFlow version: {}".format(tf.__version__))
# print("Eager execution: {}".format(tf.executing_eagerly()))

train_dataset_url = "https://storage.googleapis.com/download.tensorflow.org/data/iris_training.csv"
#get path of dataset
train_dataset_fp = tf.keras.utils.get_file(fname=os.path.basename(train_dataset_url),
                                           origin=train_dataset_url)


column_names = ['sepal_length', 'sepal_width', 'petal_length', 'petal_width', 'species']
feature_names = column_names[0: -1]
label_name = column_names[-1]
class_names = ['Iris setosa', 'Iris versicolor', 'Iris virginica']

batch_size = 32

train_dataset = tf.data.experimental.make_csv_dataset(
    train_dataset_fp,#path to data
    batch_size,
    column_names=column_names,
    label_name=label_name,
    num_epochs=1)

features, labels = next(iter(train_dataset))
# print(features)
#
# print('labels :', labels)

# plt.scatter(features['petal_length'],
#             features['sepal_length'],
#             c=labels,
#             cmap='viridis')
#
# plt.xlabel("Petal length")
# plt.ylabel("Sepal length")
# plt.show()


def pack_features_vector(features, labels):
    """Pack the features into a single array."""
    features = tf.stack(list(features.values()), axis=1)
    return features, labels


train_dataset = train_dataset.map(pack_features_vector)

features, labels = next(iter(train_dataset))

# print(features[:5].numpy())
# print(labels[:5])

model = keras.Sequential([
    layers.Dense(10, activation='relu', input_shape=(4, )),
    layers.Dense(10, activation='relu'),
    layers.Dense(3, activation='softmax')
])

# predictions = model(features)
# print(predictions)
#
# print("Prediction: {}".format(tf.argmax(predictions, axis=1)))
# print("    Labels: {}".format(labels))

#training
loss_object = keras.losses.SparseCategoricalCrossentropy(from_logits=True)

# def loss(model, x, y):
#     y_ = model(x)
#     return loss_object(y_true=y, y_pred=y_)

def loss(model, x, y, training):
  # training=training is needed only if there are layers with different
  # behavior during training versus inference (e.g. Dropout).
  y_ = model(x, training=training)

  return loss_object(y_true=y, y_pred=y_)


l = loss(model, features, labels, False)
print('loss: ', l)


def grad(model, inputs, targets):
    with tf.GradientTape() as tape:
        loss_value = loss(model, inputs, targets, training=True)
    return loss_value, tape.gradient(loss_value, model.trainable_variables)#(loss_funtion, gradiend)


#loss_value, tape_test = grad(model, features, labels)

optimizer = keras.optimizers.SGD(learning_rate=0.01)

train_loss_results = []
train_accuracy_results = []

epoch = 2000

for e in range(epoch):
    epoch_loss_avg = keras.metrics.Mean()
    epoch_accuracy = keras.metrics.SparseCategoricalAccuracy()
    batch_index = 0
    for x, y in train_dataset:
        #apply SGD
        loss_value, grads = grad(model, x, y)
        optimizer.apply_gradients(zip(grads, model.trainable_variables))
        #track change
        epoch_loss_avg.update_state(loss_value)
        epoch_accuracy.update_state(y, model(x, training=True))

    train_loss_results.append(epoch_loss_avg.result())
    train_accuracy_results.append(epoch_accuracy.result())

    # if e > 20:
    #     if train_accuracy_results[-1] - train_accuracy_results[-5] < 0.01:
    #         break

    if e % 10 == 0:
        print('Epoch {:03d}: loss: {:.3f}, accuracy: {:.3f}'.format(e, epoch_loss_avg.result(), epoch_accuracy.result()))


# fig, axes = plt.subplots(2, sharex=True, figsize=(12, 8))
# fig.suptitle('Training Metrics')
#
# axes[0].set_ylabel("Loss", fontsize=14)
# axes[0].plot(train_loss_results)
#
# axes[1].set_ylabel("Accuracy", fontsize=14)
# axes[1].set_xlabel("Epoch", fontsize=14)
# axes[1].plot(train_accuracy_results)
# plt.show()


#model.save('model.h5')


print('a')

